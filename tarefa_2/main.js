$(document).ready(function(){
    var clicked = true;
    $(".imagem").click(function(){
        if (clicked){
            $(this).animate({
                height: "+=100px",
                width: "+=100px"
            });
            clicked = false;
        }
        else{
            $(this).animate({
                height: "-=100px",
                width: "-=100px"
            });
            clicked = true;
        }
    });
  });